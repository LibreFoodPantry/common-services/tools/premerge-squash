# premerge-squash

This is a script to squash the commits in the current (feature) branch in
preparation for merging into main.

## Requirements

- bash
- git

## Local Usage

The commands can be run from anywhere. They are shown as running from the
root of the project. Replace `featureBranch` with the name of the feature
branch in which to squash commits.

```bash
git checkout featureBranch
commands/premerge-squash.sh
```

An editor window will open where you can type the commit message for the
squashed commits. Once you close your editor, the commit will complete.

Execute the following command to force push the commits to
the feature branch on the origin:

```bash
git push --force origin $(git branch --show-current)
```

## CI Usage

Not designed to be used in CI.

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
